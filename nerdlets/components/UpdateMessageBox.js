// IMPORT LIBRARIES
import React, { Component } from 'react'
import axios from 'axios'

// EXPORT COMPONENT DEFINITION
export default class UpdateMessageBox extends Component {
   // COMPONENT CONSTRUCTOR
   constructor(props) {
      super(props)
      this.state = {
         message: '',
         current: 'MessageBoxGreenState'
      }
   }

   // COMPONENT LIFECYCLE METHODS
   componentDidMount() {
      this.ChangeMessageBoxState()
      this.FetchMessageFromDataBase()
   }

   UpdateMessage = () => {
      axios.post('https://insights-collector.newrelic.com/v1/accounts/3001208/events',
      [
         {
            "eventType": 'MessageBox',
            "message": this.state.message
         }
      ],
      {
         headers: {
            'Content-Type': 'application/json',
            'X-Insert-Key': 'NRII-u73ZKvGjEnaQK9RgCkRJsgaWN4zewqBL'
         }
      })
      .then(() => {
         alert('Message successfully changed!')
      })
   }

   ChangeMessageBoxState = () => {
      setInterval(() => {
         const { state: { current } } = this
         let newCurrent = ''
         if (current === 'MessageBoxGreenState') {
            newCurrent = 'MessageBoxYellowState'
         } else if (current === 'MessageBoxYellowState') {
            newCurrent = 'MessageBoxRedState'
         } else {
            newCurrent = 'MessageBoxGreenState'
         }
         this.setState({
            current: newCurrent
         })
      }, 2000)
   }

   OnInputChange = (event) => {
      this.setState({
         message: event.target.value
      })
   }

   FetchMessageFromDataBase = () => {
      axios.get('https://insights-api.newrelic.com/v1/accounts/3001208/query', {
         headers: {
            'X-Query-Key': 'NRIQ-TqvXUjI88jElXZF3I9xbpZuVRh3Z_Bhn',
            'Accept': 'application/json',
            'Content-Type': 'application/json'
         },
         params: {
            'nrql': 'SELECT * FROM MessageBox'
         }
      },)
      .then((res) => {
         alert(JSON.stringify(res))
      })
      .catch((error) => {
         alert(JSON.stringify(error))
      })
   }

   // COMPONENT RENDERS
   render() {
      const { state } = this
      return (
         <div className="MessageBoxContainer">
            <div className={`MessageBoxStateContainer ${state.current}`} />
            <div className="TextInputContainer">
               <input
                  className="TextInputItem"
                  type="text"
                  placeholder="Message to Update"
                  onChange={ this.OnInputChange }
                  onBlur={ this.UpdateMessage }
               />
            </div>
         </div>
      )
   }
}
