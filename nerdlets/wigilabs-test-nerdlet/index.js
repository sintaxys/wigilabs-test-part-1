// IMPORT LIBRARIES
import React, { Fragment } from 'react'

// IMPORT COMPONENTS
import UpdateMessageBox from '../components/UpdateMessageBox'

// EXPORT MAIN NERDLET COMPONENT
export default class WigilabsTestNerdletNerdlet extends React.Component {
    // COMPONENT RENDERS
    render() {
        return (
            <Fragment>
                <UpdateMessageBox />
            </Fragment>
        )
    }
}
